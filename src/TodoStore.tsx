import {log} from "util";
import {autorun, computed, observable} from "mobx";

export interface ITodo {
    assignee: string,
    completed: boolean,
    task: string,
}
const NOT_ASSIGNED = 'Not assigned';
export class ObservableTodoStore {
    @observable public todos: ITodo[] = [];
    @observable public pendingRequests = 0;

    constructor() {
        autorun(() => log(this.report));
    }

    @computed get completedTodosCount() {
        return this.todos.filter(
            todo => todo.completed === true
        ).length;
    }

    @computed get report() {
        if (this.todos.length === 0){
            return "<none>";
        }
        return `Next todo: "${this.todos[0].task}". ` +
            `Progress: ${this.completedTodosCount}/${this.todos.length}`;
    }

    public addTodo(task: string | null) {
        if (task !== null) {
            this.todos.push({
                assignee: NOT_ASSIGNED,
                completed: false,
                task
            });
        }
    }
}


export const observableTodoStore = new ObservableTodoStore();
