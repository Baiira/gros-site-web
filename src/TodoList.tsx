import {observer} from "mobx-react";
import * as React from "react";
import {ITodo, ObservableTodoStore} from "./TodoStore";
import {TodoView} from "./TodoView";

interface ITodoStore {
    store: ObservableTodoStore
}

@observer
export class TodoList extends React.Component<ITodoStore> {
    public render() {
        const store = this.props.store;
        return (
            <div>
                { store.report }
                <ul>
                    { store.todos.map(
                        (todo: ITodo, idx: number) => <TodoView todo={ todo } key={ idx } />
                    ) }
                </ul>
                { store.pendingRequests > 0 ? 'Loading...' : null }
                <button onClick={ this.onNewTodo }>New Todo</button>
                <small> (double-click a todo to edit)</small>
            </div>
        );
    }

    private onNewTodo = () => {
        this.props.store.addTodo(prompt('Enter a new todo:','coffee plz'));
    }
}