import * as React from 'react';
import './App.css';

import {TodoList} from "./TodoList";
import {observableTodoStore} from "./TodoStore";

class App extends React.Component {
  public render() {
    return (
        <TodoList store={ observableTodoStore } />
    );
  }
}

export default App;
