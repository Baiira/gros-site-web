import {observer} from "mobx-react";
import * as React from "react";
import {ITodo} from "./TodoStore";

interface ITodoView {
    todo: ITodo
}

@observer
export class TodoView extends React.Component<ITodoView> {
    public render() {
        const todo: ITodo = this.props.todo;
        return (
            <li onDoubleClick={this.onRename}>
                <input
                    type='checkbox'
                    checked={todo.completed}
                    onChange={this.onToggleCompleted}
                />
                    {todo.task}
                <p>
                    {todo.assignee
                        ? <small>{todo.assignee}</small>
                        : null
                    }
                </p>
            </li>
        );
    }

    public onToggleCompleted = () => {
        const todo = this.props.todo;
        todo.completed = !todo.completed;
    };

    public onRename = () => {
        const todo = this.props.todo;
        todo.task = prompt('Task name', todo.task) || todo.task;
    }
}
